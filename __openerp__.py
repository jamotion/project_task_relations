# -*- coding: utf-8 -*-
#
#    Jamotion GmbH, Your Odoo implementation partner
#    Copyright (C) 2013-2017 Jamotion GmbH.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#    Created by Sebastien Pulver on 2017-04-12.
#
{
    'name': 'Project Task Relations',
    'version': '8.0.1.0.0',
    'category': 'Category',
    # Odoo Main Categories to select:
    # ===============================
    # Accounting & Finance, Association, Authentication
    # Base
    # Customer Relationship Management
    # Generic Modules / Accounting, Generic Modules / Inventory Control
    # Hardware Drivers, Hidden
    # Human Resources
    # Knowledge Management
    # Localization, Localization / Account Charts, Localization / Payroll
    # Managing vehicles and contracts, Manufacturing, Marketing
    # Point of Sale, Portal, Project Management, Purchase Management
    # Reporting
    # Sale, Sales Management, Social Network, Specific Industry Applications,
    # Stock Management
    # Tests, Tools
    # Warehouse Management, Website
    'author': 'Jamotion GmbH',
    'website': 'https://jamotion.ch',
    'summary': 'Create task relations to define the way they interact. '
               'Depending on the chosen relation type, your tasks will behave '
               'differently, when they move along your project workflow '
               'stages.',
    'images': [],
    'depends': [
        'project',
    ],
    'data': [
        'security/ir.model.access.csv',
        'wizard/task_relation_create_view.xml',
        'views/project_view.xml',
        'data/data_task_relation_type.xml',
        'data/data_task_relation_email_template.xml',
    ],
    'demo': [],
    'test': [],
    'application': True,
    'active': False,
    'installable': True,
}


# -*- coding: utf-8 -*-
#
#    Jamotion GmbH, Your Odoo implementation partner
#    Copyright (C) 2013-2017 Jamotion GmbH.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#    Created by Sebastien Pulver on 19.04.2017.
#
# imports of python lib
import logging

# imports of openerp
from openerp import models, fields, api, _
from openerp.exceptions import ValidationError

# imports from odoo modules

# global variable definitions
_logger = logging.getLogger(__name__)


class TaskRelation(models.Model):
    # Private attributes
    _name = 'task.relation'
    _description = 'Task Relation'
    _order = 'id'

    # Default methods

    # Fields declaration
    date_deadline_rel_task = fields.Date(
        related='rel_task_id.date_deadline',
    )

    progress_rel_task = fields.Float(
        related='rel_task_id.progress',
    )

    planned_hours_rel_task = fields.Float(
        related='rel_task_id.planned_hours',
    )

    relation_id = fields.Many2one(
        comodel_name='task.relation',
        string='Relation ID',
        ondelete="cascade",
    )

    rel_task_id = fields.Many2one(
        related='relation_id.task_id',
    )

    remaining_hours_rel_task = fields.Float(
        related='rel_task_id.remaining_hours',
    )

    stage_id_rel_task = fields.Many2one(
        related='rel_task_id.stage_id',
    )

    task_id = fields.Many2one(
        comodel_name='project.task',
        string='Task ID',
        ondelete='cascade',
    )

    type_id = fields.Many2one(
        comodel_name='task.relation.type',
        string='Relation Type',
        ondelete='cascade',
    )

    user_id = fields.Many2one(
        related='task_id.user_id',
    )

    user_id_rel_task = fields.Many2one(
        related='rel_task_id.user_id',
    )

    # compute and search fields, in the same order as fields declaration

    # Constraints and onchanges

    # CRUD methods

    # Action methods

    # Business methods
    @api.model
    def check_only_one_parent_per_task(self, task_id, rel_task_id, rel_type):
        """
        A child task may only have one parent task. This method ensures this
        and raises an error, if this constraint not respected.
        :param task_id: the id of the current task
        :param rel_task_id: the id of the related task
        :param rel_type: the id of the relation type
        """
        type_child_of = self.env.ref(
            'project_task_relations.data_task_relation_type_child'
        )
        type_parent_of = self.env.ref(
            'project_task_relations.data_task_relation_type_parent'
        )
        if rel_type == type_child_of and task_id.relation_ids.filtered(
                lambda f: f.type_id == type_child_of):
            _logger.warning(
                u'JAMO: task already has parent'
            )
            raise ValidationError(
                _(u'This task is already linked to a parent!')
            )

        if rel_type == type_parent_of and rel_task_id.relation_ids.filtered(
                lambda f: f.type_id == type_child_of):
            _logger.warning(
                u'JAMO: related task already has parent'
            )
            raise ValidationError(
                _(u'The task {0} already has a parent task!').format(
                    rel_task_id.name)
            )

    @api.model
    def check_only_one_original_task(self, task_id, rel_task_id, rel_type):
        """
        A duplicate task must only have one original task. This method ensures
        this and raises an error, if this constraint is not respected.
        :param task_id: the id of the current task
        :param rel_task_id: the id of the related task
        :param rel_type: the id of the relation type
        """
        type_original_of = self.env.ref(
            'project_task_relations.data_task_relation_type_original'
        )
        type_duplicate_of = self.env.ref(
            'project_task_relations.data_task_relation_type_duplicate'
        )
        if rel_type == type_duplicate_of and task_id.relation_ids.filtered(
                lambda f: f.type_id == type_duplicate_of):
            _logger.warning(
                u'JAMO: task is already duplicate of another task'
            )
            raise ValidationError(
                _(u'This task is already duplicate of another task!')
            )

        if rel_type == type_original_of and rel_task_id.relation_ids.filtered(
                lambda f: f.type_id == type_duplicate_of):
            _logger.warning(
                u'JAMO: related task is already duplicate of another task'
            )
            raise ValidationError(
                _(u'The task {0} already is a duplicate of another task!')
                .format(rel_task_id.name)
            )

    @api.model
    def check_recursive_relation(self, task_id, rel_task_id):
        """
        This method checks, if there is already a nested relation between the
        tasks that should be linked
        :param task_id: the id of the current task
        :param rel_task_id: the id of the related task
        """

        def get_existing_relations(tasks, relation_ids):
            for task in tasks:
                if task.relation_ids:
                    rel_task_ids = task.relation_ids.filtered(
                        lambda f: f.id not in relation_ids
                    ).mapped('relation_id.task_id')
                    relation_ids += rel_task_ids.mapped('relation_ids.id')
                    get_existing_relations(rel_task_ids, relation_ids)

        relation_ids = task_id.relation_ids.mapped('id')
        rel_tasks = task_id.relation_ids.mapped('relation_id.task_id')
        get_existing_relations(rel_tasks, relation_ids)
        relations = self.env['task.relation'].browse(relation_ids)
        for relation in relations:
            if rel_task_id == relation.task_id:
                _logger.error(
                    u'JAMO: process aborted to avoid recursive relation'
                )
                raise ValidationError(_(u'Forbidden! This would create a '
                                        u'recursive relation!'))

    @api.model
    def check_double_relation(self, task_id, rel_task_id):
        """
        There must only be one relation between two tasks. This method ensures
        this and raises an error, if this constraint is not respected.
        :param task_id: the id of the current task
        :param rel_task_id: the id of the related task
        """
        if rel_task_id in task_id.relation_ids.mapped('relation_id.task_id'):
            _logger.warning(
                u'JAMO: task connection not possible: existing relation'
            )
            raise ValidationError(_(u'The tasks are already connected to each '
                                    u'other!'))

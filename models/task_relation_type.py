# -*- coding: utf-8 -*-
#
#    Jamotion GmbH, Your Odoo implementation partner
#    Copyright (C) 2013-2015 Jamotion GmbH.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#    Created by Sebastien Pulver on 19.04.2017.
#
# imports of python lib
import logging

# imports of openerp
from openerp import models, fields, api, _

# imports from odoo modules

# global variable definitions
_logger = logging.getLogger(__name__)


class TaskRelationType(models.Model):
    # Private attributes
    _name = 'task.relation.type'
    _description = 'Relation between Tasks'

    # Default methods

    # Fields declaration
    name = fields.Char(
        string="Name",
        required=True,
        translate=True,
    )

    rel_type_id = fields.Many2one(
            comodel_name="task.relation.type",
            string="Inverse Relation",
    )

    # compute and search fields, in the same order as fields declaration

    # Constraints and onchanges

    # CRUD methods

    # Action methods

    # Business methods
    @api.multi
    def get_rel_type(self):
        """
        This method looks up the inverse relation of a task
        :return: relation type of related task
        """
        self.ensure_one()
        if self.rel_type_id:
            return self.rel_type_id

        res = self.search([('rel_type_id', '=', self.id)], limit=1)
        if res:
            return res
        return self

# -*- coding: utf-8 -*-
#
#    Jamotion GmbH, Your Odoo implementation partner
#    Copyright (C) 2013-2017 Jamotion GmbH.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#    Created by Sebastien Pulver on 19.04.2017.
#
# imports of python lib
import logging

# imports of openerp
from openerp import models, fields, api, _

# imports from odoo modules

# global variable definitions
_logger = logging.getLogger(__name__)


class TaskRelationCreate(models.TransientModel):
    # Private attributes
    _name = 'task.relation.create'
    _description = 'Create Relations between Tasks'

    # Default methods
    def _default_task_id(self):
        return self.env['project.task'].browse(
                self.env.context.get('active_id'))

    # Fields declaration
    task_id = fields.Many2one(
            comodel_name="project.task",
            string="Current Task",
            default=_default_task_id,
            readonly=True,
    )

    rel_task_id = fields.Many2one(
            comodel_name="project.task",
            string="Related Task",
            help="Choose the task you want to create a relation to",
    )

    type_id = fields.Many2one(
            comodel_name="task.relation.type",
            string="Relation Type",
            required=True,
    )

    # compute and search fields, in the same order as fields declaration

    # Constraints and onchanges

    # CRUD methods

    # Action methods

    # Business methods
    @api.multi
    def create_relation(self):
        self.ensure_one()
        self.env['task.relation'].check_double_relation(
                self.task_id,
                self.rel_task_id
        )
        self.env['task.relation'].check_recursive_relation(
                self.task_id,
                self.rel_task_id,
        )
        self.env['task.relation'].check_only_one_parent_per_task(
                self.task_id,
                self.rel_task_id,
                self.type_id
        )
        self.env['task.relation'].check_only_one_original_task(
                self.task_id,
                self.rel_task_id,
                self.type_id
        )

        vals = {
            'task_id': self.task_id.id,
            'type_id': self.type_id.id,
        }
        task_rel_1 = self.env['task.relation'].create(vals)

        vals = {
            'task_id': self.rel_task_id.id,
            'type_id': self.type_id.get_rel_type().id,
        }
        task_rel_2 = self.env['task.relation'].create(vals)

        task_rel_1.write({'relation_id': task_rel_2.id})
        task_rel_2.write({'relation_id': task_rel_1.id})

        self._set_blockers()
        self.post_chatter_message(task_rel_1, task_rel_2)
        _logger.info('JAMO: task relation created')

    @api.multi
    def _set_blockers(self):
        type_blocker_of = self.env.ref(
                'project_task_relations.data_task_relation_type_blocker')
        type_blocked_by = self.env.ref(
                'project_task_relations.data_task_relation_type_blocked')
        for record in self:
            if record.type_id == type_blocker_of:
                record.rel_task_id.kanban_state = 'blocked'
            if record.type_id == type_blocked_by:
                record.task_id.kanban_state = 'blocked'

    def post_chatter_message(self, task_rel_1, task_rel_2):
        self.task_id.message_post(
                body=_(u'Task Relation created<br /><br />'
                       u'Task {0} is now {1} Task {2}').format(
                        self.task_id.name,
                        task_rel_1.type_id.name,
                        self.rel_task_id.name),
        )
        self.rel_task_id.message_post(
                body=_(u'Task Relation created<br /><br />'
                       u'Task {0} is now {1} Task {2}').format(
                        self.rel_task_id.name,
                        task_rel_2.type_id.name,
                        self.task_id.name),
        )

# -*- coding: utf-8 -*-
#
#    Jamotion GmbH, Your Odoo implementation partner
#    Copyright (C) 2013-2017 Jamotion GmbH.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#    Created by Sebastien Pulver on 18.04.2017.
#
# imports of python lib
import logging

# imports of openerp
from openerp import models, fields

# imports from odoo modules

# global variable definitions
_logger = logging.getLogger(__name__)


class ProjectTaskType(models.Model):
    # Private attributes
    _description = 'Task Stage'
    _inherit = 'project.task.type'

    # Default methods

    # Fields declaration
    use_backlog = fields.Boolean(
            string="Use as Backlog",
    )

    use_todo = fields.Boolean(
            string="Use as To Do",
    )

    use_done = fields.Boolean(
            string="Use as Done",
    )

    # compute and search fields, in the same order as fields declaration

    # Constraints and onchanges

    # CRUD methods

    # Action methods

    # Business methods

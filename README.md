Project Task Relations
======================

Create task relations to define the way they interact. Depending on the chosen
relation type, your tasks will behave differently, when they move along your
project workflow stages.

Check the [documentation](https://bitbucket.org/jamotion/
project_task_relations/src/8.0/doc/index.rst?fileviewer=file-view-default)
for further information...
# -*- coding: utf-8 -*-
#
#    Jamotion GmbH, Your Odoo implementation partner
#    Copyright (C) 2013-2017 Jamotion GmbH.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#    Created by Sebastien Pulver on 25.04.2017.
#
import logging

import openerp
import openerp.tests

from openerp.tests import common
from openerp.exceptions import ValidationError

_logger = logging.getLogger(__name__)


class TestDuplicateRelation(common.TransactionCase):
    def setUp(self):
        super(TestDuplicateRelation, self).setUp()
        self.task_stage_no_flag = self.env['project.task.type'].create({
            'name': 'Do',
        })
        self.test_task_01 = self.env['project.task'].create({
            'name': 'Test Task 01',
        })
        self.test_task_02 = self.env['project.task'].create({
            'name': 'Test Task 02',
        })
        self.test_task_03 = self.env['project.task'].create({
            'name': 'Test Task 03',
        })
        self.type_id = self.browse_ref(
            'project_task_relations.data_task_relation_type_original')

    def test_move_original_with_multiple_duplicates(self):
        # When the original task is moved to another stage, the duplicated
        # tasks should follow
        self._create_duplicate_relation(self.test_task_01, self.test_task_02)
        self._create_duplicate_relation(self.test_task_01, self.test_task_03)

        self.test_task_01.stage_id = self.task_stage_no_flag

        self.assertEqual(self.test_task_02.stage_id, self.task_stage_no_flag)
        self.assertEqual(self.test_task_03.stage_id, self.task_stage_no_flag)

        # with self.assertRaises(ValidationError):
        #     raise ValidationError

    def _create_duplicate_relation(self, task_1, task_2):
        vals = {
            'task_id': task_1.id,
            'type_id': self.type_id.id,
        }
        task_rel_1 = self.env['task.relation'].create(vals)

        vals = {
            'task_id': task_2.id,
            'type_id': self.type_id.get_rel_type().id,
        }
        task_rel_2 = self.env['task.relation'].create(vals)
        task_rel_1.write({'relation_id': task_rel_2.id})
        task_rel_2.write({'relation_id': task_rel_1.id})
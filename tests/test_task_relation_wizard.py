# -*- coding: utf-8 -*-
#
#    Jamotion GmbH, Your Odoo implementation partner
#    Copyright (C) 2013-2017 Jamotion GmbH.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#    Created by sebi.pulver on 21.04.2017.
#
import logging

from openerp.tests import common

_logger = logging.getLogger(__name__)


class TestTaskRelationWizard(common.TransactionCase):
    def setUp(self):
        super(TestTaskRelationWizard, self).setUp()
        self.type_relation = self.browse_ref(
                'project_task_relations.data_task_relation_type_default')
        self.type_predecessor = self.browse_ref(
                'project_task_relations.data_task_relation_type_predecessor')
        self.type_successor = self.browse_ref(
                'project_task_relations.data_task_relation_type_successor')
        self.type_parent_of = self.browse_ref(
                'project_task_relations.data_task_relation_type_parent')
        self.type_child_of = self.browse_ref(
                'project_task_relations.data_task_relation_type_child')
        self.type_blocker_of = self.browse_ref(
                'project_task_relations.data_task_relation_type_blocker')
        self.type_blocked_by = self.browse_ref(
                'project_task_relations.data_task_relation_type_blocked')
        self.type_duplicate = self.browse_ref(
                'project_task_relations.data_task_relation_type_duplicate')
        self.type_original_of = self.browse_ref(
                'project_task_relations.data_task_relation_type_original')

        self.test_task_01 = self.env['project.task'].create({
            'name': 'Test Task 01',
        })
        self.test_task_02 = self.env['project.task'].create({
            'name': 'Test Task 02',
        })
        self.test_task_03 = self.env['project.task'].create({
            'name': 'Test Task 03',
        })
        self.test_wizard = self.env['task.relation.create'].create({
            'task_id': self.test_task_01.id,
            'rel_task_id': self.test_task_02.id,
            'type_id': self.type_relation.id,
        })

    def test_create_simple_relation(self):
        self.test_wizard.create_relation()

        relations = self.test_task_01.relation_ids.filtered(
                lambda f: f.type_id == self.type_relation)

        self.assertEqual(len(self.test_task_01.relation_ids), 1)
        self.assertTrue(relations[0].relation_id)
        self.assertEqual(relations[0].relation_id.task_id,
                         self.test_task_02)
        self.assertEqual(relations[0].relation_id.type_id.id,
                         self.type_relation.id)

    def test_create_predecessor_of_relation(self):
        self.test_wizard['type_id'] = self.type_predecessor
        self.test_wizard.create_relation()

        relations = self.test_task_01.relation_ids.filtered(
                lambda f: f.type_id == self.type_predecessor)

        self.assertEqual(len(self.test_task_01.relation_ids), 1)
        self.assertTrue(relations[0].relation_id)
        self.assertEqual(relations[0].relation_id.task_id, self.test_task_02)
        self.assertEqual(relations[0].relation_id.type_id.id,
                         self.type_successor.id)

    def test_create_successor_of_relation(self):
        self.test_wizard['type_id'] = self.type_successor
        self.test_wizard.create_relation()

        relations = self.test_task_01.relation_ids.filtered(
                lambda f: f.type_id == self.type_successor)

        self.assertEqual(len(self.test_task_01.relation_ids), 1)
        self.assertTrue(relations[0].relation_id)
        self.assertEqual(relations[0].relation_id.task_id, self.test_task_02)
        self.assertEqual(relations[0].relation_id.type_id.id,
                         self.type_predecessor.id)

    def test_create_parent_of_relation(self):
        self.test_wizard['type_id'] = self.type_parent_of
        self.test_wizard.create_relation()

        relations = self.test_task_01.relation_ids.filtered(
                lambda f: f.type_id == self.type_parent_of)

        self.assertEqual(len(self.test_task_01.relation_ids), 1)
        self.assertTrue(relations[0].relation_id)
        self.assertEqual(relations[0].relation_id.task_id,
                         self.test_task_02)
        self.assertEqual(relations[0].relation_id.type_id.id,
                         self.type_child_of.id)

    def test_create_child_of_relation(self):
        self.test_wizard['type_id'] = self.type_child_of
        self.test_wizard.create_relation()

        relations = self.test_task_01.relation_ids.filtered(
                lambda f: f.type_id == self.type_child_of)

        self.assertEqual(len(self.test_task_01.relation_ids), 1)
        self.assertTrue(relations[0].relation_id)
        self.assertEqual(relations[0].relation_id.task_id, self.test_task_02)
        self.assertEqual(relations[0].relation_id.type_id.id,
                         self.type_parent_of.id)

    def test_create_blocker_of_relation(self):
        self.test_wizard['type_id'] = self.type_blocker_of
        self.test_wizard.create_relation()

        relations = self.test_task_01.relation_ids.filtered(
                lambda f: f.type_id == self.type_blocker_of)

        self.assertEqual(len(self.test_task_01.relation_ids), 1)
        self.assertTrue(relations[0].relation_id)
        self.assertEqual(relations[0].relation_id.task_id, self.test_task_02)
        self.assertEqual(relations[0].relation_id.type_id.id,
                         self.type_blocked_by.id)

    def test_create_blocked_by_relation(self):
        self.test_wizard['type_id'] = self.type_blocked_by
        self.test_wizard.create_relation()

        relations = self.test_task_01.relation_ids.filtered(
                lambda f: f.type_id == self.type_blocked_by)

        self.assertEqual(len(self.test_task_01.relation_ids), 1)
        self.assertTrue(relations[0].relation_id)
        self.assertEqual(relations[0].relation_id.task_id, self.test_task_02)
        self.assertEqual(relations[0].relation_id.type_id.id,
                         self.type_blocker_of.id)

    def test_create_duplicate_of_relation(self):
        self.test_wizard['type_id'] = self.type_duplicate
        self.test_wizard.create_relation()

        relations = self.test_task_01.relation_ids.filtered(
                lambda f: f.type_id == self.type_duplicate)

        self.assertEqual(len(self.test_task_01.relation_ids), 1)
        self.assertTrue(relations[0].relation_id)
        self.assertEqual(relations[0].relation_id.task_id, self.test_task_02)
        self.assertEqual(relations[0].relation_id.type_id.id,
                         self.type_original_of.id)

    def test_create_original_of_relation(self):
        self.test_wizard['type_id'] = self.type_original_of
        self.test_wizard.create_relation()

        relations = self.test_task_01.relation_ids.filtered(
                lambda f: f.type_id == self.type_original_of)

        self.assertEqual(len(self.test_task_01.relation_ids), 1)
        self.assertTrue(relations[0].relation_id)
        self.assertEqual(relations[0].relation_id.task_id, self.test_task_02)
        self.assertEqual(relations[0].relation_id.type_id.id,
                         self.type_duplicate.id)

Project Task Relations
======================

Create task relations to define the way they interact. Depending on the chosen
relation type, your tasks will behave differently, when they move along your
project workflow stages.

Changelog
=========

Version 8.0.1.0.0
-----------------
Initial release
# -*- coding: utf-8 -*-
#
#    Jamotion GmbH, Your Odoo implementation partner
#    Copyright (C) 2013-2017 Jamotion GmbH.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#    Created by Sebastien Pulver on 21.04.2017.
#
import logging

import openerp
import openerp.tests

from openerp.tests import common
from openerp.exceptions import ValidationError

_logger = logging.getLogger(__name__)


class TestSequenceRelation(common.TransactionCase):
    def setUp(self):
        super(TestSequenceRelation, self).setUp()
        self.task_stage_backlog = self.env['project.task.type'].create({
            'name': 'Backlog',
            'use_backlog': True,
        })
        self.task_stage_todo = self.env['project.task.type'].create({
            'name': 'ToDo',
            'use_todo': True,
        })
        self.task_stage_done = self.env['project.task.type'].create({
            'name': 'Done',
            'use_done': True,
        })
        self.task_stage_cancelled = self.env['project.task.type'].create({
            'name': 'Cancelled',
            'use_done': True,
        })
        self.task_stage_no_flag = self.env['project.task.type'].create({
            'name': 'Do',
        })
        self.test_project = self.env['project.project'].create({
            'name': 'Test Project 01',
            'type_ids': [
                (4, self.task_stage_backlog.id),
                (4, self.task_stage_todo.id),
                (4, self.task_stage_done.id),
                (4, self.task_stage_cancelled.id),
                (4, self.task_stage_no_flag.id),
            ]
        })
        self.test_task_01 = self.env['project.task'].create({
            'name': 'Test Task 01',
            'project_id': self.test_project.id,
        })
        self.test_task_02 = self.env['project.task'].create({
            'name': 'Test Task 02',
            'project_id': self.test_project.id,
        })
        self.test_task_03 = self.env['project.task'].create({
            'name': 'Test Task 03',
            'project_id': self.test_project.id,
        })
        self.type_predecessor = self.browse_ref(
                'project_task_relations.data_task_relation_type_predecessor')

    def test_move_stage_to_no_flag(self):
        self._create_sequence_relation(self.test_task_01, self.test_task_02)
        self.test_task_02.stage_id = self.task_stage_todo
        self.test_task_01.stage_id = self.task_stage_no_flag

        self.assertTrue(self.test_task_02.stage_id.use_todo)

    def test_move_stage_to_done(self):
        self._create_sequence_relation(self.test_task_01, self.test_task_02)
        self.test_task_01.stage_id = self.task_stage_done

        self.assertTrue(self.test_task_02.stage_id.use_todo)

    def test_move_stage_back_to_todo(self):
        self._create_sequence_relation(self.test_task_01, self.test_task_02)
        self.test_task_01.stage_id = self.task_stage_done
        self.test_task_01.stage_id = self.task_stage_todo

        self.assertTrue(self.test_task_02.stage_id.use_todo)

    def test_multiple_successors(self):
        self._create_sequence_relation(self.test_task_01, self.test_task_02)
        self._create_sequence_relation(self.test_task_01, self.test_task_03)

        self.test_task_01.stage_id = self.task_stage_done
        self.assertTrue(self.test_task_02.stage_id.use_todo)
        self.assertTrue(self.test_task_03.stage_id.use_todo)

    def _create_sequence_relation(self, task_1, task_2):
        vals = {
            'task_id': task_1.id,
            'type_id': self.type_predecessor.id,
        }
        task_rel_1 = self.env['task.relation'].create(vals)

        vals = {
            'task_id': task_2.id,
            'type_id': self.type_predecessor.get_rel_type().id,
        }
        task_rel_2 = self.env['task.relation'].create(vals)
        task_rel_1.write({'relation_id': task_rel_2.id})
        task_rel_2.write({'relation_id': task_rel_1.id})

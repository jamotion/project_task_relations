Project Task Relations
======================

Create task relations to define the way they interact. Depending on the chosen
relation type, your tasks will behave differently, when they move along your
project workflow stages.

The following chapters will give you some inputs about configuration and usage
of the features:

Different types of task relation
--------------------------------

Usage
^^^^^

Tasks can be connected in the following ways:

Type "Relation"
---------------
A task is related to an other. This relation type is not bound to any automatic
processing. It just informs about the tasks being linked.

Type "Sequence"
---------------
Tasks are linked as predecessors and successors. When a task with successors
is moved to a stage marked as "done", the successor tasks are moved to the next
stage defined as "to do".

Type "Hierarchy"
----------------
In hierarchical relations, tasks are linked as parents and children. A parent
task can have multiple children, whereas a child belongs to only one parent.

When the first child task is moved to a stage marked as "to do", the parent
task will follow. As soon as the last child task reaches a stage marked as
"done", the parent task will also move to the next stage marked as "done".

Restrictions: A task declared as child can only have one parent task.

Type "Blocker"
--------------
A task related as blocker sets the kanban state of the blocked task to
"blocked". When the last blocker task changes state to "done", the kanban state
of the blocked task becomes "Ready for next stage".

Type "Duplicate"
----------------
Tasks can be declared as duplicates of an other task. The duplicates will be
moved to the current stage of the original task, whenever it changes.
A task declared as original can have multiple duplicates.

Restrictions: A task can only be duplicate of one original task

Configuration Options
^^^^^^^^^^^^^^^^^^^^^

Todo's
------
- Move all child tasks to next stage marked as "to do", when parent is moved to
  such a stage

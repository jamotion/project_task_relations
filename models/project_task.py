# -*- coding: utf-8 -*-
#
#    Jamotion GmbH, Your Odoo implementation partner
#    Copyright (C) 2013-2017 Jamotion GmbH.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#    Created by Sebastien Pulver on 18.04.2017.
#
# imports of python lib
import logging

# imports of openerp
from openerp import models, fields, api

# imports from odoo modules

# global variable definitions
_logger = logging.getLogger(__name__)


class Task(models.Model):
    # Private attributes
    _description = 'Task'
    _inherit = 'project.task'
    
    # Default methods
    
    # Fields declaration
    display_name = fields.Char(
        string="Display Name",
            compute='_compute_display_name',
    )
    
    relation_ids = fields.One2many(
            comodel_name="task.relation",
            inverse_name="task_id",
            string="Related Tasks",
    )
    
    key = fields.Char(
            string="Key",
            compute='_compute_key',
    )
    
    # compute and search fields, in the same order as fields declaration
    @api.multi
    def _compute_key(self):
        for record in self:
            if record.project_id and record.project_id.code:
                record.key = u'{0}-{1}'.format(
                        record.project_id.code, record.id)
            else:
                record.key = u'NONE-{0}'.format(record.id)
    
    @api.multi
    def _compute_display_name(self):
        for record in self:
            record.display_name = u'[{0}] {1}'.format(record.key, record.name)
            
    # Constraints and onchanges
    
    # CRUD methods
    
    @api.multi
    def write(self, vals):
        """
        Overwrite method to handle automatic stage changes of related tasks
        :param vals: dictionary of changed task values
        """
        result = super(Task, self).write(vals)
        if 'stage_id' in vals:
            self._change_stage_of_related_tasks()
        return result
        
        # Action methods
        
        # Business methods
    
    @api.multi
    def _change_stage_of_related_tasks(self):
        for record in self:
            record._handle_blocker()
            record._handle_hierarchy()
            record._handle_sequence()
            record._handle_duplicate()
    
    @api.multi
    def _handle_blocker(self):
        """
        This method changes the kanban state of a blocked task to "done", when
        a blocker task is moved to a "done" stage
        """
        self.ensure_one()
        blocker_of_rel = self.env.ref(
                'project_task_relations.data_task_relation_type_blocker')
        blocked_by_rel = self.env.ref(
                'project_task_relations.data_task_relation_type_blocked')
        blocked_tasks_rel = self.relation_ids.filtered(
                lambda f: f.type_id == blocker_of_rel)
        if not blocked_tasks_rel or not self.stage_id.use_done:
            return
        
        blocked_tasks = blocked_tasks_rel.mapped('relation_id.task_id')
        
        for task in blocked_tasks:
            res = task.relation_ids.filtered(
                    lambda f: f.type_id == blocked_by_rel
                              and not f.relation_id.task_id.stage_id.use_done)
            if not res:
                task.kanban_state = 'done'
                task.send_notification_mail()
    
    def _handle_hierarchy(self):
        """
        This method handles the automatic stage changes of the parent task,
        when a child task is moved to a "todo" or a "done" stage
        """
        self.ensure_one()
        child_of_rel = self.env.ref(
                'project_task_relations.data_task_relation_type_child')
        parent_of_rel = self.env.ref(
                'project_task_relations.data_task_relation_type_parent')
        child_relation = self.relation_ids.filtered(
                lambda f: f.type_id == child_of_rel)
        if not child_relation:
            return
        
        parent = child_relation.relation_id.task_id
        other_children_relations = parent.relation_ids \
            .filtered(lambda f: f.type_id == parent_of_rel)
        if self.stage_id.use_todo:
            parent.stage_id = self.stage_id
            parent.send_notification_mail()
        
        if self.stage_id.use_done:
            all_done = True
            for ocr in other_children_relations:
                if not ocr.rel_task_id.stage_id.use_done:
                    all_done = False
            
            if all_done:
                parent.stage_id = next(stage for stage in
                                       parent.project_id.type_ids
                                       if stage.use_done)
                parent.send_notification_mail()
    
    def _handle_sequence(self):
        """
        This method handles the automatic stage change of successor tasks, when
        the predecessor task is moved to a "done" stage
        """
        successor_of_rel = self.env.ref(
                'project_task_relations.data_task_relation_type_successor')
        if not self.stage_id.use_done:
            return
        
        relations = self.relation_ids.mapped('relation_id')
        
        successor_relations = relations.filtered(
                lambda f: f.task_id.stage_id.use_backlog
                          and f.type_id == successor_of_rel)
        
        for successor_rel in successor_relations:
            stage = next(stage for stage in
                         successor_rel.task_id.project_id.type_ids
                         if stage.use_todo)
            
            successor_rel.task_id.stage_id = stage.id
            successor_rel.task_id.send_notification_mail()
    
    def _handle_duplicate(self):
        """
        This method handles the automatic stage changing of duplicate tasks,
        when the original task is moved
        """
        original_of_rel = self.env.ref(
                'project_task_relations.data_task_relation_type_original')
        duplicates_rel = self.relation_ids.filtered(
                lambda f: f.type_id == original_of_rel)
        if not duplicates_rel:
            return
        
        duplicate_tasks = duplicates_rel.mapped('relation_id.task_id')
        
        for task in duplicate_tasks:
            task.stage_id = self.stage_id
    
    @api.multi
    def send_notification_mail(self):
        """
        Send notification email to followers of the task based on mail template
        :return:
        """
        self.ensure_one()
        template = self.env.ref(
                'project_task_relations.email_template_task_relation')
        template.send_mail(self.id)
